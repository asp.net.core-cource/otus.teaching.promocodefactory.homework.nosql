﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.Administration.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [BsonElement("firstName")]
        public string FirstName { get; set; }

        [BsonElement("lastName")]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [BsonElement("email")]
        public string Email { get; set; }

        public Guid RoleId { get; set; }

        [BsonElement("role")]
        public virtual Role Role { get; set; }

        [BsonElement("appliedPromocodesCount")]
        public int AppliedPromocodesCount { get; set; }
    }
}