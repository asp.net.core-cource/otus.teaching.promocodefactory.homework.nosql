﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Repositories
{
	public class MongoRepository<T>
		: IRepository<T>
		where T : BaseEntity
	{
		private readonly IMongoCollection<T> _collection;
		public MongoRepository(MongoClient mongoClient)
		{
			var database = mongoClient.GetDatabase("givingToCustomer");
			_collection = database.GetCollection<T>("givingToCustomer");
		}
		public async Task AddAsync(T entity)
		{
			await _collection.InsertOneAsync(entity);
		}

		public async Task DeleteAsync(T entity)
		{
			await _collection.DeleteOneAsync(e => e.Id == e.Id);
		}

		public async Task<IEnumerable<T>> GetAllAsync()
		{
			var result = await _collection.FindAsync(_ => true);
			return result.ToList();
		}

		public async Task<T> GetByIdAsync(Guid id)
		{
			var result = await _collection.FindAsync(c => c.Id == id);
			return result.FirstOrDefault();
		}

		public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
		{
			var result = await _collection.FindAsync(predicate);
			return result.FirstOrDefault();
		}

		public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
		{
			if(ids?.Count < 1)
				return null;

			var result = await _collection.FindAsync(c => ids.Contains(c.Id));
			return result.ToList();
		}

		public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
		{
			var result = await _collection.FindAsync(predicate);
			return result.ToList();
		}

		public async Task UpdateAsync(T entity)
		{
			var options = new FindOneAndReplaceOptions<T>
			{
				ReturnDocument = ReturnDocument.After
			};

			await _collection.FindOneAndReplaceAsync<T>(x => x.Id == entity.Id, entity, options);
		}
	}
}
