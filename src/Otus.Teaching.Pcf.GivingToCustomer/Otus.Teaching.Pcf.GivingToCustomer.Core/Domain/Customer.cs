﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Customer
        :BaseEntity
    {
        [BsonElement("firstName")]
        public string FirstName { get; set; }

        [BsonElement("lastName")]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [BsonElement("email")]

        public string Email { get; set; }

        [BsonElement("preferences")]

        public virtual ICollection<CustomerPreference> Preferences { get; set; }

        [BsonElement("promoCodes")]

        public virtual ICollection<PromoCodeCustomer> PromoCodes { get; set; }
    }
}