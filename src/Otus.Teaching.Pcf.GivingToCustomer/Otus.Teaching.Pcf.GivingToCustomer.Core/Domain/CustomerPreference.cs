﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class CustomerPreference
    {
        public Guid CustomerId { get; set; }

        [BsonElement("customer")]
        public virtual Customer Customer { get; set; }

        public Guid PreferenceId { get; set; }

        [BsonElement("preference")]
        public virtual Preference Preference { get; set; }
    }
}