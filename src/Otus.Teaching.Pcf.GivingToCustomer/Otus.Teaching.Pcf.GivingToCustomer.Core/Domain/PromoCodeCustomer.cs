﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCodeCustomer : BaseEntity
    {
        public Guid PromoCodeId { get; set; }

        [BsonElement("promoCode")]
        public virtual PromoCode PromoCode { get; set; }

        public Guid CustomerId { get; set; }

        [BsonElement("customer")]
        public virtual Customer Customer { get; set; }
    }
}
