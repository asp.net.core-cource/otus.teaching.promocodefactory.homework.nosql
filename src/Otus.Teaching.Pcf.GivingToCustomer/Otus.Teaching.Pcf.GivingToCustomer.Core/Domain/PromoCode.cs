﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class PromoCode
        : BaseEntity
    {
        [BsonElement("code")]
        public string Code { get; set; }

        [BsonElement("serviceInfo")]

        public string ServiceInfo { get; set; }


        [BsonElement("beginDate")]

        public DateTime BeginDate { get; set; }

        [BsonElement("endDate")]

        public DateTime EndDate { get; set; }

        [BsonElement("partnerId")]

        public Guid PartnerId { get; set; }

        [BsonElement("preference")]

        public virtual Preference Preference { get; set; }

        public Guid PreferenceId { get; set; }

        [BsonElement("customers")]

        public virtual ICollection<PromoCodeCustomer> Customers { get; set; }
    }
}