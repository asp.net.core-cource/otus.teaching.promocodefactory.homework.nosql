﻿using MongoDB.Bson.Serialization.Attributes;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Domain
{
    public class Preference
        :BaseEntity
    {
        [BsonElement("name")]
        public string Name { get; set; }
    }
}